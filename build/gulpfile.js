const gulp = require('gulp'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    srcPath = __dirname + '/../src/**/*.js',
    distPath = __dirname + '/../dist';

gulp.task('default', function () {
    return gulp.src(srcPath)
        .pipe(concat('scripts.js'))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest(distPath))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(distPath));
});

gulp.task('watch', function () {
    gulp.watch(srcPath, ['default']);
});