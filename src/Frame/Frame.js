class VTFrame {
    constructor(width, height) {
        this.vtc = new VTCanvas(VTFrame.createCanvas(width, height));
        this.dh = new VTDrawHelper(this.vtc);
        this.blLoop = true;
        return this;
    }

    static createCanvas(width, height) {
        width = width || 800;
        height = height || 600;
        let c = document.createElement('canvas');
        c.height = height;
        c.width = width;
        document.body.appendChild(c);
        return c;
    }

    initListener() {
        document.addEventListener('keydown', this.keyPressed.bind(this));
    }

    start() {
        this.initListener();
        this.setup();
        this.loop();
    }

    loop() {
        this.draw();
        if (this.blLoop) {
            requestAnimationFrame(this.loop.bind(this));
        }

    }

    loopMode(boolean) {
        this.blLoop = boolean;
    }

    keyPressed(event) {
        //nothing
    }

    draw() {
    }

    setup() {
    }

    getHeight() {
        return this.vtc.canvas.height;
    }

    getWidth() {
        return this.vtc.canvas.width;
    }
}