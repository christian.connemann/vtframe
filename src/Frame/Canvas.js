class VTCanvas {
    constructor(canvas, mode) {
        mode = mode || '2d';
        this.canvas = canvas;
        this.context = canvas.getContext(mode);
        this.init();
    }

    init() {
        this.config = {
            stroke: true,
            fill: false,
            colorMode: 'rgba',
            alpha: false
        }
    }

    colorMode(mode) {
        mode = mode || 'rgba';
        this.config.colorMode = mode;
        return this;
    }

    fillColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        let color = Utils.getColor(a, b, c, d, mode, this.config.alpha);
        if (mode !== 'hex')
            color = `${mode}(${color})`;
        context.fillStyle = color;
        return this;
    }

    strokeColor(a, b, c, d) {
        let context = this.context;
        let mode = this.config.colorMode;
        let color = Utils.getColor(a, b, c, d, mode, this.config.alpha);
        if (mode !== 'hex')
            color = `${mode}(${color})`;
        context.strokeStyle = color;
        return this;
    }

    alpha(boolean) {
        this.config.alpha = boolean;
        return this;
    }

    strokeWeight(weight) {
        this.context.lineWidth = weight;
        return this;
    }

    stroke(boolean) {
        this.config.stroke = boolean;
        return this;
    }

    fill(boolean) {
        this.config.fill = boolean;
        return this;
    }

    draw() {
        let context = this.context;
        let config = this.config;
        if (config.stroke)
            context.stroke();
        if (config.fill)
            context.fill();
        return this;
    }

    background() {
        let context = this.context;
        context.beginPath();
        context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        context.closePath();
        return this;
    }
}