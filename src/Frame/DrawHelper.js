class VTDrawHelper {
    constructor(VTCanvas) {
        this.canvas = VTCanvas;
    }

    start() {
        let context = this.canvas.context;
        context.beginPath();
        return context;
    }

    end(context) {
        this.canvas.draw();
        context.closePath();
    }

    ellipse(x, y, r1, r2) {
        let context = this.start();
        context.ellipse(x, y, r1, r2, 0, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    circle(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        this.end(context);
        return this;
    }

    point(x, y, d) {
        let context = this.start();
        context.arc(x, y, d, 0, 2 * Math.PI);
        context.fill();
        context.closePath();
        return this;
    }

    rect(x, y, w, h) {
        let context = this.start();
        context.rect(x, y, w, h);
        this.end(context);
        return this;
    }

    fromTo(x1, y1, x2, y2) {
        let context = this.start();
        context.moveTo(x1, y1);
        context.lineTo(x2, y2);
        this.end(context);
        return this;
    }

    fromToCurved(x1, y1, x2, y2) {
        let context = this.start();
        let x_mid = (x1 + x2) / 2;
        let y_mid = (y1 + y2) / 2;
        let cp_x1 = (x_mid + x1) / 2;
        let cp_x2 = (x_mid + x2) / 2;
        context.quadraticCurveTo(cp_x1, y1, x_mid, y_mid);
        context.quadraticCurveTo(cp_x2, y2, x2, y2);
        this.end(context);
        return this;
    }
}