class Utils {
    static getColor(a, b, c, alpha, mode, useAlpha) {
        if (mode === 'hex') {
            a = a || '#FFF';
            return a;
        }
        a = a || '0';
        b = b || '0';
        c = c || '0';
        alpha = alpha || '1';

        let color = a + ',' + b + ',' + c;
        if (useAlpha) {
            color += ',' + alpha;
        }
        return color;
    }
}