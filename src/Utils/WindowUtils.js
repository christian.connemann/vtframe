window.random = function (min, max) {
    let rand = Math.random();
    if (typeof min === 'undefined') {
        return rand;
    } else if (typeof max === 'undefined') {
        if (min instanceof Array) {
            return min[Math.floor(rand * min.length)];
        } else {
            return rand * min;
        }
    } else {
        if (min > max) {
            let tmp = min;
            min = max;
            max = tmp;
        }
        return rand * (max - min) + min;
    }
};
window.normalize = function (val, max, min) {
    return (val - min) / (max - min);
};

window.noise = function (x, last) {
    return last + this.random(-x, x);
};

