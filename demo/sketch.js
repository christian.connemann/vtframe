let force = null;
let fireworks = [];
let maxVector = 0;
let maxFireworks = 10;
let vtFrame = new VTFrame(innerWidth, innerHeight);

vtFrame.setup = function () {
    let width = vtFrame.getWidth();
    let height = vtFrame.getHeight();
    fireworks.push(new Firework(random(10, width - 10), height, this));
    force = new Vector(random(-0.02, 0.02), 0.2);
    maxVector = -(findMaxVelocity(height, 60));
    maxFireworks = width / 100;
    this.vtc.alpha(true).stroke(false);
};

vtFrame.draw = function () {
    let width = vtFrame.getWidth();
    let height = vtFrame.getHeight();
    let vtc = vtFrame.vtc;

    vtc.fillColor(0, 0, 0, .5).background();
    for (let i = 0; i < fireworks.length; i++) {
        let firework = fireworks[i];
        if (firework.life.startTick !== 0) {
            firework.updateTick();
            continue;
        }
        firework.applyForce(force);
        firework.update();
        firework.show();

        if (firework.isDead()) {
            fireworks.splice(i, 1);
        }
    }

    let ran = random();
    if (ran < 0.1 && fireworks.length < maxFireworks) {
        fireworks.push(new Firework(random(width), height, this));
    }

    if (ran < 0.002) {
        force = new Vector(random(-0.02, 0.02), 0.2);
    }

};

vtFrame.start();