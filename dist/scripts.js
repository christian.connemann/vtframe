"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var VTCanvas =
/*#__PURE__*/
function () {
  function VTCanvas(canvas, mode) {
    _classCallCheck(this, VTCanvas);

    mode = mode || '2d';
    this.canvas = canvas;
    this.context = canvas.getContext(mode);
    this.init();
  }

  _createClass(VTCanvas, [{
    key: "init",
    value: function init() {
      this.config = {
        stroke: true,
        fill: false,
        colorMode: 'rgba',
        alpha: false
      };
    }
  }, {
    key: "colorMode",
    value: function colorMode(mode) {
      mode = mode || 'rgba';
      this.config.colorMode = mode;
      return this;
    }
  }, {
    key: "fillColor",
    value: function fillColor(a, b, c, d) {
      var context = this.context;
      var mode = this.config.colorMode;
      var color = Utils.getColor(a, b, c, d, mode, this.config.alpha);
      if (mode !== 'hex') color = "".concat(mode, "(").concat(color, ")");
      context.fillStyle = color;
      return this;
    }
  }, {
    key: "strokeColor",
    value: function strokeColor(a, b, c, d) {
      var context = this.context;
      var mode = this.config.colorMode;
      var color = Utils.getColor(a, b, c, d, mode, this.config.alpha);
      if (mode !== 'hex') color = "".concat(mode, "(").concat(color, ")");
      context.strokeStyle = color;
      return this;
    }
  }, {
    key: "alpha",
    value: function alpha(boolean) {
      this.config.alpha = boolean;
      return this;
    }
  }, {
    key: "strokeWeight",
    value: function strokeWeight(weight) {
      this.context.lineWidth = weight;
      return this;
    }
  }, {
    key: "stroke",
    value: function stroke(boolean) {
      this.config.stroke = boolean;
      return this;
    }
  }, {
    key: "fill",
    value: function fill(boolean) {
      this.config.fill = boolean;
      return this;
    }
  }, {
    key: "draw",
    value: function draw() {
      var context = this.context;
      var config = this.config;
      if (config.stroke) context.stroke();
      if (config.fill) context.fill();
      return this;
    }
  }, {
    key: "background",
    value: function background() {
      var context = this.context;
      context.beginPath();
      context.fillRect(0, 0, this.canvas.width, this.canvas.height);
      context.closePath();
      return this;
    }
  }]);

  return VTCanvas;
}();

var VTDrawHelper =
/*#__PURE__*/
function () {
  function VTDrawHelper(VTCanvas) {
    _classCallCheck(this, VTDrawHelper);

    this.canvas = VTCanvas;
  }

  _createClass(VTDrawHelper, [{
    key: "start",
    value: function start() {
      var context = this.canvas.context;
      context.beginPath();
      return context;
    }
  }, {
    key: "end",
    value: function end(context) {
      this.canvas.draw();
      context.closePath();
    }
  }, {
    key: "ellipse",
    value: function ellipse(x, y, r1, r2) {
      var context = this.start();
      context.ellipse(x, y, r1, r2, 0, 0, 2 * Math.PI);
      this.end(context);
      return this;
    }
  }, {
    key: "circle",
    value: function circle(x, y, d) {
      var context = this.start();
      context.arc(x, y, d, 0, 2 * Math.PI);
      this.end(context);
      return this;
    }
  }, {
    key: "point",
    value: function point(x, y, d) {
      var context = this.start();
      context.arc(x, y, d, 0, 2 * Math.PI);
      context.fill();
      context.closePath();
      return this;
    }
  }, {
    key: "rect",
    value: function rect(x, y, w, h) {
      var context = this.start();
      context.rect(x, y, w, h);
      this.end(context);
      return this;
    }
  }, {
    key: "fromTo",
    value: function fromTo(x1, y1, x2, y2) {
      var context = this.start();
      context.moveTo(x1, y1);
      context.lineTo(x2, y2);
      this.end(context);
      return this;
    }
  }, {
    key: "fromToCurved",
    value: function fromToCurved(x1, y1, x2, y2) {
      var context = this.start();
      var x_mid = (x1 + x2) / 2;
      var y_mid = (y1 + y2) / 2;
      var cp_x1 = (x_mid + x1) / 2;
      var cp_x2 = (x_mid + x2) / 2;
      context.quadraticCurveTo(cp_x1, y1, x_mid, y_mid);
      context.quadraticCurveTo(cp_x2, y2, x2, y2);
      this.end(context);
      return this;
    }
  }]);

  return VTDrawHelper;
}();

var VTFrame =
/*#__PURE__*/
function () {
  function VTFrame(width, height) {
    _classCallCheck(this, VTFrame);

    this.vtc = new VTCanvas(VTFrame.createCanvas(width, height));
    this.dh = new VTDrawHelper(this.vtc);
    this.blLoop = true;
    return this;
  }

  _createClass(VTFrame, [{
    key: "initListener",
    value: function initListener() {
      document.addEventListener('keydown', this.keyPressed.bind(this));
    }
  }, {
    key: "start",
    value: function start() {
      this.initListener();
      this.setup();
      this.loop();
    }
  }, {
    key: "loop",
    value: function loop() {
      this.draw();

      if (this.blLoop) {
        requestAnimationFrame(this.loop.bind(this));
      }
    }
  }, {
    key: "loopMode",
    value: function loopMode(boolean) {
      this.blLoop = boolean;
    }
  }, {
    key: "keyPressed",
    value: function keyPressed(event) {//nothing
    }
  }, {
    key: "draw",
    value: function draw() {}
  }, {
    key: "setup",
    value: function setup() {}
  }, {
    key: "getHeight",
    value: function getHeight() {
      return this.vtc.canvas.height;
    }
  }, {
    key: "getWidth",
    value: function getWidth() {
      return this.vtc.canvas.width;
    }
  }], [{
    key: "createCanvas",
    value: function createCanvas(width, height) {
      width = width || 800;
      height = height || 600;
      var c = document.createElement('canvas');
      c.height = height;
      c.width = width;
      document.body.appendChild(c);
      return c;
    }
  }]);

  return VTFrame;
}();

var Utils =
/*#__PURE__*/
function () {
  function Utils() {
    _classCallCheck(this, Utils);
  }

  _createClass(Utils, null, [{
    key: "getColor",
    value: function getColor(a, b, c, alpha, mode, useAlpha) {
      if (mode === 'hex') {
        a = a || '#FFF';
        return a;
      }

      a = a || '0';
      b = b || '0';
      c = c || '0';
      alpha = alpha || '1';
      var color = a + ',' + b + ',' + c;

      if (useAlpha) {
        color += ',' + alpha;
      }

      return color;
    }
  }]);

  return Utils;
}();

var Vector =
/*#__PURE__*/
function () {
  function Vector(x, y, z) {
    _classCallCheck(this, Vector);

    this.x = x || 0;
    this.y = y || 0;
    this.z = z || 0;
  } //helper


  _createClass(Vector, [{
    key: "mult",
    value: function mult(times) {
      this.x *= times;
      this.y *= times;
      this.z *= times;
    }
  }, {
    key: "set",
    value: function set(vector) {
      this.x = vector.x;
      this.y = vector.y;
      this.z = vector.z;
    }
  }, {
    key: "add",
    value: function add(vector) {
      this.x = this.x + vector.x;
      this.y = this.y + vector.y;
      this.z = this.z + vector.z;
    }
  }, {
    key: "addXYZ",
    value: function addXYZ(x, y, z) {
      this.x = x || 0;
      this.y = y || 0;
      this.z = z || 0;
      this.x = this.x + x;
      this.y = this.y + y;
      this.z = this.z + z;
    }
  }, {
    key: "clone",
    value: function clone() {
      return new Vector(this.x, this.y, this.z);
    }
  }], [{
    key: "createRandom",
    value: function createRandom(x, y, z) {
      x = x || 1;
      y = y || 1;
      z = z || 0;
      return new Vector(random(-x, x), random(-y, y), random(-z, z));
    }
  }]);

  return Vector;
}();

window.random = function (min, max) {
  var rand = Math.random();

  if (typeof min === 'undefined') {
    return rand;
  } else if (typeof max === 'undefined') {
    if (min instanceof Array) {
      return min[Math.floor(rand * min.length)];
    } else {
      return rand * min;
    }
  } else {
    if (min > max) {
      var tmp = min;
      min = max;
      max = tmp;
    }

    return rand * (max - min) + min;
  }
};

window.normalize = function (val, max, min) {
  return (val - min) / (max - min);
};

window.noise = function (x, last) {
  return last + this.random(-x, x);
};